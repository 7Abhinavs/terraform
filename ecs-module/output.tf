output "securitygroup-id" {
  value = aws_security_group.myapp-elb-securitygroup.id
}

output "cluster-name" {
  value = aws_ecs_cluster.example-cluster.id
}

output "iam-arn"{
  value = aws_iam_role.ecs-service-role.arn
}

output "ecs-sg" {
  value = aws_security_group.ecs-securitygroup.id
}