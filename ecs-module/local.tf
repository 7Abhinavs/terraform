locals {
  vpc_id = data.terraform_remote_state.main.outputs.vpc_id
}

locals {
  task-definition-arn = data.terraform_remote_state.myapp.outputs.task-definition-name
}

locals {
  elb-name = data.terraform_remote_state.myapp-elb.outputs.alb-name
}

# locals {
#   ecs-securitygrp = data.terraform_remote_state.ecs-securitygroup.outputs.ecs-sg
# }

locals {
  subnet-1a = data.terraform_remote_state.main.outputs.subnet-id-1a
}

locals {
  subnet-1b = data.terraform_remote_state.main.outputs.subnet-id-1b
}