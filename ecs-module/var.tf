variable "ECS_AMIS" {
  type = map(string)
  default = {
    ap-south-1 = "ami-08db0e1881e30be69"
  }
}


variable "ECS_INSTANCE_TYPE" {
  default = "t2.micro"
}


variable "AWS_REGION" {
  default = "ap-south-1"
}