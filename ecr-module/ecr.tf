resource "aws_ecr_repository" "myapp" {
  name = var.ecr-name
  force_delete = true
}