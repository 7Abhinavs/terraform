# app
data "terraform_remote_state" "myapp" {
  backend = "local"
  config = {
    path = "../ecr-module/terraform.tfstate"
  }
} 

data "template_file" "myapp-task-definition-template" {
  template = file("../templates/app.json.tpl")
  vars = {
    REPOSITORY_URL = replace(local.ecr-url, "https://", "")
  }
}

resource "aws_ecs_task_definition" "myapp-task-definition" {
  family                = var.family
  container_definitions = data.template_file.myapp-task-definition-template.rendered
}
