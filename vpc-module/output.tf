output "vpc_id" {
  value = aws_vpc.main.id
}

output "subnet-id-1a" {
  value = aws_subnet.main-public-1.id
}

output "subnet-id-1b" {
  value = aws_subnet.main-public-2.id
}

output "subnet-id-1c" {
  value = aws_subnet.main-public-3.id
}

output "private-subnet-1a" {
  value = aws_subnet.main-private-1.id
}

output "private-subnet-2a" {
  value = aws_subnet.main-private-2.id
}

output "private-subnet-3a" {
  value = aws_subnet.main-private-3.id
}