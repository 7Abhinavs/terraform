variable "listener-port" {
  default = 3000
}

variable "listener-protocol" {
    type = string
  default ="http"
}


variable "listener-lb-port" {
  default = 80
}

variable "listener-lb-protocol" {
    type = string
  default = "http"
}

variable "health-check-threshold" {
    default = 3
  
}

variable "Unhealthy-check-threshold" {
  default = 3
}

variable "health-check-target" {
    type = string
    default = "HTTP:300/"
  
}