locals {
  subnet-1a = data.terraform_remote_state.main.outputs.subnet-id-1a
}

locals {
  subnet-1b = data.terraform_remote_state.main.outputs.subnet-id-1b
}

locals {
  subnet-1c = data.terraform_remote_state.main.outputs.subnet-id-1c
}

locals {
  security_group-id = data.terraform_remote_state.myapp-elb-securitygroup.outputs.securitygroup-id
}