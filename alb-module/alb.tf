data "terraform_remote_state" "main" {
  backend = "local"
  config = {
    path = "../vpc-module/terraform.tfstate"
  }
}
data "terraform_remote_state" "myapp-elb-securitygroup" {
  backend = "local"
  config = {
    path = "../ecs-module/terraform.tfstate"
  }
}


resource "aws_elb" "myapp-elb" {
  name = "myapp-elb"

  listener {
    instance_port     = var.listener-port
    instance_protocol = var.listener-protocol
    lb_port           = var.listener-lb-port
    lb_protocol       = var.listener-lb-protocol
  }

  health_check {
    healthy_threshold   = var.health-check-threshold
    unhealthy_threshold = var.Unhealthy-check-threshold
    timeout             = 30
    target              = var.health-check-target
    interval            = 60
  }

  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  subnets         = [local.subnet-1a]
  security_groups = [local.security_group-id]

  tags = {
    Name = "myapp-elb"
  }
}

