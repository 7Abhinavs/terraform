locals {
  cluster-name = data.terraform_remote_state.example-cluster.outputs.cluster-name
}

locals {
  task-definition-arn = data.terraform_remote_state.myapp-task-definition.outputs.task-definition-name
}

locals {
  ecs-service-role = data.terraform_remote_state.ecs-service-role.outputs.iam-arn
}

locals {
  elb-name = data.terraform_remote_state.myapp-elb.outputs.alb-name
}