data "terraform_remote_state" "example-cluster" {
  backend = "local"
  config = {
    path = "../ecs-module/terraform.tfstate"
  }
} 


data "terraform_remote_state" "myapp-task-definition" {
  backend = "local"
  config = {
    path = "../app-module/terraform.tfstate"
  }
} 


data "terraform_remote_state" "ecs-service-role" {
  backend = "local"
  config = {
    path = "../ecs-module/terraform.tfstate"
  }
} 

data "terraform_remote_state" "myapp-elb" {
  backend = "local"
  config = {
    path = "../alb-module/terraform.tfstate"
  }
} 



resource "aws_ecs_service" "myapp-service" {
  name            = "myapp"
  cluster         = local.cluster-name
  task_definition = local.task-definition-arn
  desired_count   = 1
  iam_role        = local.ecs-service-role
  # depends_on      = [aws_iam_policy_attachment.ecs-service-attach1]

  load_balancer {
    elb_name       = local.elb-name
    container_name = "myapp"
    container_port = 3000
  }
  lifecycle {
    ignore_changes = [task_definition]
  }
}