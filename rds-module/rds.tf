data "terraform_remote_state" "main" {
  backend = "local"
  config = {
    path = "../vpc-module/terraform.tfstate"
  }
}

data "terraform_remote_state" "ecs-securitygroup" {
  backend = "local"
  config = {
    path = "../ecs-module/terraform.tfstate"
  }
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"

  identifier = "demodb"

  engine            = "mysql"
  engine_version    = "5.7.25"
  instance_class    = "db.t3a.large"
  allocated_storage = 5

  db_name  = "demodb"
  username = "user"
  password = "abhinav6"
  port     = "3306"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [local.ecs-sg]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
  monitoring_interval = "30"
  monitoring_role_name = "MyRDSMonitoringRole"
  create_monitoring_role = true

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
  create_db_subnet_group = true
  subnet_ids             = [local.private-subnet-1a, local.private-subnet-2a]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Database Deletion Protection
  deletion_protection = true

}