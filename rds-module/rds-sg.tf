resource "aws_db_security_group" "rds-sg" {
  name = "rds_sg"

  ingress {
    cidr = local.ecs-sg
  }
}