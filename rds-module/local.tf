locals {
  private-subnet-1a = data.terraform_remote_state.main.outputs.private-subnet-1a
}

locals {
  private-subnet-2a = data.terraform_remote_state.main.outputs.private-subnet-1b
}


locals {
  ecs-sg = data.terraform_remote_state.ecs-securitygroup.securitygroup-id
}